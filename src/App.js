import { Fragment } from 'react';
import { Container } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import AppNavbar from './components/AppNavbar';
import Home from './Pages/Home'

import './App.css'

function App() {
  return (
    // In React JS, multiple components rendered in a single component should be wrapped in a parent components
    // "Fragment" ensures that an error will be prevented
    <>
      <AppNavbar/>
      <Home/>



    </>
  );
}

export default App;