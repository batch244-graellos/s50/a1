import { Navbar, Container, Nav} from 'react-bootstrap'


export default function AppNavbar() {
	return(
		<Navbar bg="light" expand="lg">
		<Container>
        <Navbar.Brand href="#home">Zuitt Booking</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link href="#home">Home</Nav.Link>
            <Nav.Link href="#link">Courses</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
      </Navbar>
		)
}

// export default AppNavbar (can also be used)