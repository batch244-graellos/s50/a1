import {Container} from 'react-bootstrap';

import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import Coursecard from '../components/Coursecard';

export default function Home(){
	return (
		<>
			<Container>
			    <Banner />
			    <Highlights />
			    <Coursecard />
			</Container>
		</>
	)
}